+++
title= "Reproduction: truth or multiplicity?"
date= Date(2025,01,15)
+++

# {{fill title}}

\figenv{René Magritt – La reproduction interdite, 1937.}{/assets/Magritte1937.jpg}{width:100%;}

In diatom taxonomy you don't understand species. You just get used to them.