+++
title= "Genus Cestodiscus: A Commonplace"
date= Date(2025,01,17)
+++

# {{fill title}}

`last update:` 2025-01-17 10:45

Life feels too short to keep dealing with this group and its tangled mess. It’s odd, given how often the literature has pointed out that some species within this group are almost indistinguishable –barely distinguishable even in those carefully curated taxonomical appendices featuring either painfully low-quality photos or polished, stacked images. My aim here is to document the persistent confusion surrounding this group, as noted in prior publications. Somehow, I already sense that my future self will have some intense internal debates about the time invested in what, at this point, feels like borderline nonsense. Let's go!

• Barron et al. (2004) raises the following: "_These forms (C. reticulatus) closely resemble Cestodiscus robustus except for the reticulate pattern in the valve’s center, and it is not clear that they should be separated._"

• Strelnikova et al. (2000) says: "_The flatter specimens (Cestodiscus sp. 2 of Fenner) are very similar to Cestodiscus fennerae (see above), while forms with concave valve face resemble Cestodiscus trochus. Forms domed with small concave center are similar to C. convexus. Al these morphologies (taxa) are very closely related to one another and might be conspecific._" 
    - They are indeed conspecific. 

• Mikkelsen and Barron (1977): "_These forms (Cestodiscus reticulatus) closely resemble C. gemmifer except for the reticulate pattern in the center, and it is not clear that they should be separated._" 
    - When combined with Barron et al. 2004; C. reticulatus, C. robustus, C. gemmifer are kinda same species. 

• Strelnikova et al. (2000): "_It (Cestodiscus trochus) is very close ot Cestodiscus reticulatus Fenner._"
    - It is maybe because they are the same species.

My note here is the following: There are many more examples, and this list will (hopefully not) be updated regularly. Yet, although spending time on this, when some of us are trying to think beyond the paradigm they were born into, looks extremely ridiculus, these uncertainties, buried beneath the crushing weight of neglect, underlies a concept called stratigraphically(or temporally)-informed-taxonomy, where you decide the species when you see a Cestodiscus based on the temporal frame of these confused species documented by Strelnikova et al. 2000. Wuh, irritatingly tangled mess of writing. But I don't have power to elaborate on this.

\figenv{Sit tibi terra levis, büyük usta.}{/assets/lynch.jpeg}{width:100%;}