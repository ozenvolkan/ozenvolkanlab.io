+++
title= "Peace"
date= Date(2024,07,27)
+++

# {{fill title}}

I read Cuvier in 2007, and I have no peace since that day.

\figenv{Théobald Chartran's work depicts an extremely focused Baron Cuvier.}{/assets/cuvier.jpg}{width:100%;}