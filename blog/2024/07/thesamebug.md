@def title = "The same bugs" 
@def tags = ["taxonomy", "diatom"] 
@def isblog = true

# {{fill title}}

Genus _Cestodiscus_ is probably one of those pain-in-the-ass groups, yet beautifuly(? citation needed) ornamented.

Today's note is on the following: _Cestodiscus reticulatus_ (below) was described by the great Fenner (1984). It's also one of the key bugs in the hill I die on: "_Cestodiscus reticulatus_ and _Cestodiscus robustus_ (yup, also below) are the same species". 

\figenv{C. reticulatus and C. robustus from ODP 748-12-05,107 –scale 20 µm}{/assets/diatoms/Creticulatus.jpg}{width:50%;border: 1px solid gray;}

\figenv{The hill}{/assets/diatoms/thehill.webp}{width:50%}

Also Barron at al. 2004* note:

> These forms closely resemble _Cestodiscus robustus_ except for the reticulate pattern in the valve’s center, and it is not clear that they should be separated. (Pl. P3, fig. 6)


-----

*Barron, J.A., Fourtanier, E., Bohaty, S.M., 2004. Oligocene and Earliest Miocene Diatom Biostratigraphy of ODP Leg 199 Site 1220, Equatorial Pacific. Proceedings of the Ocean Drilling Program. Ocean Drilling Program. https://doi.org/10.2973/odp.proc.sr.199.204.2004