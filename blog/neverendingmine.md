+++
title= "A never-ending mine"
date= Date(2024,07,27)
+++

# {{fill title}}

Within the current madness of the [bullshit](https://www.everythingisbullshit.blog/) land of science, there is a deeply broken classification of what is meaningful research and what is not. Yet, before entering this deeply rotten business, as a kid, a single poorly preserved Nummulite fossil I collected from my grandparents' village and the nebulas I observed with my uncle's telescope were just different pieces of the same puzzle to me. After stepping into the modern institutions of _scientism_, whatever feelings I had developed thus far were succumbed, like Laocoön and his sons Antiphantes and Thymbraeus, and replaced by something concrete-like and disgusting.

Yet, in his biography of Balzac, the great Stefan Zweig writes the following, saving the day again and again:

> Everything can be a subject. The small, banal, and somewhat monotonous Eugénie Grandet shows as much courage by adding an extra sugar to the coffee of her cousin Charles, under the threatening gaze of her miserly father, as Napoleon did crossing the Bridge of Lodi with a flag in hand. In Père Goriot, the Vauquer Boarding House, where twelve people live together, can become as much a center of intensity as Lavoisier's laboratory or Cuvier's study.

> During these years, Balzac discovered a great secret. Everything is a subject. Once you know how to research it, reality is a never-ending mine.