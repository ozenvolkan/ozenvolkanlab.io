+++
title= "Giordano"
date= Date(2025,02,17)
+++

# {{fill title}}

\figenv{Giordano Bruno.}{/assets/bruno.jpg}{width:70%;}

They put Bruno to fire on this day in 1600, yet he still stands.

One thing is certain: Bruno was one of the greatest and most intelligent minds of the early modern period. His legacy, however, did not flow through the usual veins of intellectual history because the very idea of ‘intelligence’ has since wrapped into something smushy and snotty nerdiness. Yet “**intelligence has always been as much about virtues like honesty, integrity, and bravery as it is about raw intellect ([Nabeel Qureshi](https://nabeelqu.co/understanding))**.”

Bowing before his memory.

An excerpt from a wonderful book, _The Swerve_ (Stephen Greenblatt), captures Giordano Bruno’s final moment –terrified yet refusing to submit, emotionally and intellectually:

> Before an audience of spectators, Bruno was forced to his knees and sentenced as “an impenitent, pernicious, and obstinate heretic.” He was no Stoic; he was clearly terrified by the grisly fate that awaited him. But one of the spectators, a German Catholic, jotted down strange words that the obstinate heretic had spoken at the moment of his conviction and excommunication: "He made no other reply than, in a menacing tone, 'You may be more afraid to bring that sentence against me than I am to accept it.'"

And after 400 years, unbelievably, we have ended up in a century full of ‘_intellectual yet idiots_’ (sensu Nassim Taleb), endlessly posing on X (formerly Twitter) and seeking to build authority over knowledge instead of defending knowledge itself.
