+++
title= "The Hidden Injuries of Time"
date= Date(2024,11,30)
+++

# {{fill title}}

Things are no longer about thinking –they are about consumption. This deeply entrenched pattern reveals itself in every domain, every corner, every society, and, yes, among paleontologists who have been invited back to the high table of modern evolutionary synthesis, though it seems only as a coaster (excluding great Raup et alia). One example of this is the use of modern calibrations –linking mud-line diatom assemblages to sea-surface conditions– to infer diatom assemblages, say, from late Eocene.

This “classy” practice of scientism strips paleo-communities of their agency –as evolving entities–, reducing them to mere shadows of modern communities. It is an act of ecological classism that brings us back to our title, (more than) inspired by Sennett’s The Hidden Injuries of Class. And, since we go turbo-nuclear in analogies, this is precisely what Taleb means when he says, “Academia is to knowledge what prostitution is to love.”