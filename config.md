@def author = "Volkan Özen"

\newcommand{\R}{\mathbb R}
\newcommand{\scal}[1]{\langle #1 \rangle}

@def generate_rss = false

+++
website_url = "ozenvolkan.gitlab.io"
+++



\newcommand{\figenv}[3]{
~~~
<figure style="text-align:center;">
<img src="!#2" style="padding:0;#3" alt="#1"/>
<figcaption>#1</figcaption>
</figure>
~~~
}

