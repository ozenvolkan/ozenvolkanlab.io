+++
title = "News"
author= "Volkan Özen"
tags= ["news"]
draft= false
+++

# News

`2024-11-19 18:03` Presented a preliminary report of our EXP400 findings, highlighting the "hidden injuries" of a diatomist grappling with the challenge of reconstructing the age model of the continental-margin-the-hell.

`2024-07-06 07:35` Accepted to the legendary [Santa Fe GAINs School](https://www.santafe.edu/engage/learn/programs/complexity-gains-international-school) this year, focusing on ecological persistence and resilience. I will be there in October, representing my lovely bugs, diatoms, and working to go where the ignorance is maximum.
