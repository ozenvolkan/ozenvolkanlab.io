@def title = "About"




@@im-100
![](/assets/coverpagevvv.jpg)
@@

# About me

Merhaba, I'm Volkan Özen.

I'm a paleobiologist. I work on macroevolution of diatoms. That _Asterolamphalus_ on the picture above can actually be a new species. The problem is I don't remember from which sample I took that freaking photo. It was just random shot while trying to set my camera. I am developing my skills to the fullest in quantitative methods while trying to create solid datasets -mainly species level diatom diversity- which takes time and seems untenable while the madding crowd is scavenging the every bit of existing -yuugely incomplete and biased- data to squeze it into tantalizing hypotheses nobody can even test. Diatom taxonmy is a mess. A challenge. In the end, it doesn't have to be fun to be fun. 

You can find my CV [here](/assets/OzenVolkan2025.pdf).